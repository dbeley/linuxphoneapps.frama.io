+++
title = "Fluid"
description = "The Fluid Library."
date = 2021-08-15T08:50:45+00:00
updated = 2021-08-15T08:50:45+00:00
draft = false
+++


Fluid is a "Library for [QtQuick](@/frameworks/qtquick.md) apps with Material Design".

* [Fluid on GitHub](https://github.com/lirios/fluid)