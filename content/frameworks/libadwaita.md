+++
title = "libadwaita"
description = "Building blocks for modern GNOME applications"
date = 2021-09-07T08:50:45+00:00
updated = 2021-09-07T08:50:45+00:00
draft = false
+++

**libadwaita** describes itself as "Building blocks for modern GNOME applications". It is to [GTK4](@/frameworks/gtk4.md) what [libhandy](@/frameworks/libhandy.md) is to [GTK+ 3](@/frameworks/gtk3.md). It's still under heavy development. 

* [Docs](https://gnome.pages.gitlab.gnome.org/libadwaita/doc/)
* [Source](https://gitlab.gnome.org/GNOME/libadwaita)