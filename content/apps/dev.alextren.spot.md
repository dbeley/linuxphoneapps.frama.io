+++
title = "Spot"
description = "Native Spotify client for the Gnome desktop"
aliases = []
date = 2021-02-20
updated = 2022-12-19

[taxonomies]
project_licenses = [ "MIT",]
metadata_licenses = []
app_author = [ "xou816",]
categories = [ "audio streaming",]
mobile_compatibility = [ "5",]
status = []
frameworks = [ "GTK4", "libadwaita",]
backends = []
services = [ "Spotify",]
packaged_in = [ "alpine_3_17", "alpine_3_18", "alpine_edge", "aur", "flathub", "nix_stable_22_11", "nix_stable_23_05", "nix_unstable",]
freedesktop_categories = [ "GTK", "GNOME", "Audio", "Network", "Player",]
programming_languages = [ "Rust",]
build_systems = [ "meson",]

[extra]
repository = "https://github.com/xou816/spot"
homepage = ""
bugtracker = "https://github.com/xou816/spot/issues/"
donations = ""
translations = ""
more_information = []
summary_source_url = "https://github.com/xou816/spot"
screenshots = []
screenshots_img = []
all_features_touch = ""
intended_for_mobile = ""
app_id = "dev.alextren.Spot"
scale_to_fit = "spot"
flathub = "https://flathub.org/apps/dev.alextren.Spot"
flatpak_link = ""
flatpak_recipe = ""
snapcraft = ""
snap_link = ""
snap_recipe = ""
repology = [ "spot-client",]
appstream_xml_url = "https://raw.githubusercontent.com/xou816/spot/master/data/dev.alextren.Spot.appdata.xml"
reported_by = "linmob"
updated_by = "script"
+++







### Notice

Spotify Premium only, GTK4/libadwaita since 0.2.0
