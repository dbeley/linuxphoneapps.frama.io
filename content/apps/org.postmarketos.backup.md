+++
title = "postmarketOS Backup"
description = "A backup application that integrates with apk to quickly backup and restore your system state in postmarketOS"
aliases = []
date = 2021-06-23
updated = 2022-12-19

[taxonomies]
project_licenses = [ "LGPL-3.0-or-later",]
metadata_licenses = []
app_author = [ "postmarketOS Developers",]
categories = [ "backup", "system utilities",]
mobile_compatibility = [ "5",]
status = []
frameworks = [ "GTK3", "libhandy",]
backends = []
services = []
packaged_in = []
freedesktop_categories = [ "GTK", "GNOME", "Utility",]
programming_languages = [ "Python",]
build_systems = [ "meson",]

[extra]
repository = "https://gitlab.com/postmarketOS/postmarketos-backup"
homepage = ""
bugtracker = "https://gitlab.com/postmarketOS/postmarketos-backup/-/issues/"
donations = ""
translations = ""
more_information = [ "https://www.youtube.com/watch?v=4neYnrLgTy8",]
summary_source_url = "https://gitlab.com/postmarketOS/postmarketos-backup"
screenshots = [ "https://twitter.com/linuxphoneapps/status/1407781364406095877",]
screenshots_img = []
all_features_touch = ""
intended_for_mobile = ""
app_id = "org.postmarketos.Backup"
scale_to_fit = ""
flathub = ""
flatpak_link = ""
flatpak_recipe = ""
snapcraft = ""
snap_link = ""
snap_recipe = ""
repology = []
appstream_xml_url = "https://gitlab.com/postmarketOS/postmarketos-backup/-/raw/master/data/org.postmarketos.Backup.appdata.xml"
reported_by = "linmob"
updated_by = "script"

+++