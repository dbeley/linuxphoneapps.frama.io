+++
title = "Kompass"
description = "Compass application for plasma mobile"
aliases = []
date = 2021-12-18
updated = 2022-12-19

[taxonomies]
project_licenses = [ "GPL-2.0-or-later",]
metadata_licenses = []
app_author = [ "Sylvain Migaud",]
categories = [ "utilities",]
mobile_compatibility = [ "5",]
status = []
frameworks = [ "Kirigami",]
backends = []
services = []
packaged_in = []
freedesktop_categories = [ "Qt", "KDE", "Utilities", "Geography",]
programming_languages = [ "Cpp", "QML",]
build_systems = [ "cmake",]

[extra]
repository = "https://invent.kde.org/smigaud/kompass"
homepage = ""
bugtracker = "https://invent.kde.org/smigaud/kompass/-/issues/"
donations = ""
translations = ""
more_information = []
summary_source_url = "https://invent.kde.org/smigaud/kompass"
screenshots = []
screenshots_img = []
all_features_touch = ""
intended_for_mobile = ""
app_id = "org.kde.kompass"
scale_to_fit = ""
flathub = ""
flatpak_link = ""
flatpak_recipe = ""
snapcraft = ""
snap_link = ""
snap_recipe = ""
repology = []
appstream_xml_url = "https://invent.kde.org/smigaud/kompass/-/raw/master/org.kde.kompass.appdata.xml"
reported_by = "linmob"
updated_by = "script"

+++



### Notice

WIP Compass app
